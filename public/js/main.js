import { createApp } from './vendor/vue.esm-browser.prod.min.js'
import App from './app.js'
import cache from './client/cache.js'

cache.prune()

createApp(App).mount('#app')

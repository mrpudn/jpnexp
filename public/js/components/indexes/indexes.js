import KanjiJLPT from './kanji/jlpt.js'
import KanjiJouyou from './kanji/jouyou.js'
import KanjiJinmeiyou from './kanji/jinmeiyou.js'
import KanjiRadical from './kanji/radical.js'
import KanjiStrokes from './kanji/strokes.js'

export default {
  template: '<component :is="component" />',
  data() {
    const params = new URLSearchParams(window.location.search)
    return {
      index: params.get('index')
    }
  },
  computed: {
    component() {
      switch (this.index) {
        case 'kanji-jlpt': return KanjiJLPT
        case 'kanji-jouyou': return KanjiJouyou
        case 'kanji-jinmeiyou': return KanjiJinmeiyou
        case 'kanji-radical': return KanjiRadical
        case 'kanji-strokes': return KanjiStrokes
      }
      return NotFound
    }
  }
}

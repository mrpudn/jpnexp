import client from '../../../client.js'
import KanjiLinks from '../../kanji/links.js'

export default {
  name: 'KanjiByJouyou',
  components: {
    KanjiLinks
  },
  template: `
    <h2>Kanji by Jouyou Grade</h2>

    <div v-if="loading">Loading ...</div>

    <div v-else-if="index">
      <h3>Grade 1</h3>
      <KanjiLinks :kanji="index[1]" />

      <h3>Grade 2</h3>
      <KanjiLinks :kanji="index[2]" />

      <h3>Grade 3</h3>
      <KanjiLinks :kanji="index[3]" />

      <h3>Grade 4</h3>
      <KanjiLinks :kanji="index[4]" />

      <h3>Grade 5</h3>
      <KanjiLinks :kanji="index[5]" />

      <h3>Grade 6</h3>
      <KanjiLinks :kanji="index[6]" />

      <h3>Secondary Education</h3>
      <KanjiLinks :kanji="index[8]" />
    </div>

    <div v-else>No information available.</div>
  `,
  data() {
    return {
      loading: true,
      index: null
    }
  },
  created() {
    client.kanjidic.getIndex('jouyou')
      .then((data) => {
        this.index = {}
        data.forEach(row => {
          if (!(row[0] in this.index)) this.index[row[0]] = []
          this.index[row[0]].push(row[1])
        })
        this.loading = false
      })
  }
}

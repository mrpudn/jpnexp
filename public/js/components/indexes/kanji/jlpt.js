import client from '../../../client.js'
import KanjiLinks from '../../kanji/links.js'

export default {
  name: 'KanjiByJLPT',
  components: {
    KanjiLinks
  },
  template: `
    <h2>Kanji by JLPT Level</h2>

    <div v-if="loading">Loading ...</div>

    <div v-else-if="index">
      <h3>N4 Level</h3>
      <KanjiLinks :kanji="index[4]" />

      <h3>N3 Level</h3>
      <KanjiLinks :kanji="index[3]" />

      <h3>N2 Level</h3>
      <KanjiLinks :kanji="index[2]" />

      <h3>N1 Level</h3>
      <KanjiLinks :kanji="index[1]" />
    </div>

    <div v-else>No information available.</div>
  `,
  data() {
    return {
      loading: true,
      index: null
    }
  },
  created() {
    client.kanjidic.getIndex('jlpt')
      .then((data) => {
        this.index = {}
        data.forEach(row => {
          if (!(row[0] in this.index)) this.index[row[0]] = []
          this.index[row[0]].push(row[1])
        })
        this.loading = false
      })
  }
}

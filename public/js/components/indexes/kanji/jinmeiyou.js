import client from '../../../client.js'
import KanjiLinks from '../../kanji/links.js'

export default {
  name: 'KanjiByJinmeiyou',
  components: {
    KanjiLinks
  },
  template: `
    <h2>Jinmeiyou Kanji</h2>

    <div v-if="loading">Loading ...</div>

    <div v-else-if="index">
      <KanjiLinks :kanji="index[9]" />

      <h3>Jouyou Kanji Variants</h3>
      <KanjiLinks :kanji="index[10]" />
    </div>

    <div v-else>No information available.</div>
  `,
  data() {
    return {
      loading: true,
      index: null
    }
  },
  created() {
    client.kanjidic.getIndex('jinmeiyou')
      .then((data) => {
        this.index = {}
        data.forEach(row => {
          if (!(row[0] in this.index)) this.index[row[0]] = []
          this.index[row[0]].push(row[1])
        })
        this.loading = false
      })
  }
}

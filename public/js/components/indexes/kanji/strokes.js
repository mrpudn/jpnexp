import client from '../../../client.js'
import KanjiLinks from '../../kanji/links.js'

export default {
  name: 'KanjiByStrokes',
  components: {
    KanjiLinks
  },
  template: `
    <h2>Kanji by Stroke Count</h2>

    <div v-if="loading">Loading ...</div>

    <div v-else-if="index">
      <div v-for="count in Object.keys(index)">
        <h3 v-if="count != 1">{{ count }} Strokes</h3>
        <h3 v-else>{{ count }} Stroke</h3>

        <KanjiLinks :kanji="index[count]" />
      </div>
    </div>

    <div v-else>No information available.</div>
  `,
  data() {
    return {
      loading: true,
      index: null
    }
  },
  created() {
    client.kanjidic.getIndex('strokes')
      .then((data) => {
        this.index = {}
        data.forEach(row => {
          if (!(row[0] in this.index)) this.index[row[0]] = []
          this.index[row[0]].push(row[1])
        })
        this.loading = false
      })
  }
}

import client from '../../../client.js'
import KanjiLinks from '../../kanji/links.js'
import radicals from '../../../data/radicals.js'

export default {
  name: 'KanjiByRadical',
  components: {
    KanjiLinks
  },
  template: `
    <h2>Kanji by Radical</h2>

    <div v-if="loading">Loading ...</div>

    <div v-else-if="index">
      <div v-for="number in Object.keys(index)">
        <h3 class="japanese">
          (#{{number}})
          {{ radicals[number].radical }}
          <span v-if="radicals[number].variant">
            ({{ radicals[number].variant}})
          </span>
        </h3>

        <p>{{ radicals[number].info }}</p>

        <KanjiLinks :kanji="index[number]" />
      </div>
    </div>

    <div v-else>No information available.</div>
  `,
  data() {
    return {
      loading: true,
      index: null,
      radicals: radicals
    }
  },
  created() {
    client.kanjidic.getIndex('radical')
      .then((data) => {
        this.index = {}
        data.forEach(row => {
          if (!(row[0] in this.index)) this.index[row[0]] = []
          this.index[row[0]].push(row[1])
        })
        this.loading = false
      })
  }
}

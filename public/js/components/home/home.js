
export default {
  name: 'Home',
  template: `
    <h2>Kanji</h2>

    <input type="text" v-model="kanji" />
    <input type="button" value="Go" @click="goKanji()">

    <h3>Indexes</h3>
    <ul>
      <li><a href="?index=kanji-jouyou">Jouyou Kanji</a></li>
      <li><a href="?index=kanji-jinmeiyou">Jinmeiyou Kanji</a></li>
      <li><a href="?index=kanji-jlpt">Kanji by JLPT Level</a></li>
      <li><a href="?index=kanji-radical">Kanji by Radical</a></li>
      <li><a href="?index=kanji-strokes">Kanji by Stroke Count</a></li>
    </ul>
  `,
  methods: {
    goKanji() {
      window.location = encodeURI('?kanji=' + this.kanji)
    }
  }
}

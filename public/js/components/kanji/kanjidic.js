import client from '../../client.js'

export default {
  name: 'Kanjidic',
  props: ['kanji'],
  template: `
    <h2>
      <a href="https://gitlab.com/mrpudn/kanjidic.git">Kanjidic</a>
    </h2>

    <div v-if="loading">Loading ...</div>

    <div v-else-if="kanjidic">
      <h3>General Information</h3>
      <ul>
        <li v-if="kanjidic.jlpt">
          <strong><a href="?index=kanji-jlpt">JLPT</a>:</strong>
          {{ kanjidic.jlpt }}
        </li>
        <li v-if="kanjidic.grade && kanjidic.grade < 9">
          <strong><a href="?index=kanji-jouyou">Grade</a>:</strong>
          {{ kanjidic.grade }}
        </li>
        <li v-if="kanjidic.grade && kanjidic.grade > 8">
          <strong><a href="?index=kanji-jinmeiyou">Grade</a>:</strong>
          Jinmeiyou
        </li>
        <li v-if="kanjidic.strokes">
          <strong><a href="?index=kanji-strokes">Strokes</a>:</strong>
          {{ kanjidic.strokes }}
        </li>
        <li v-if="kanjidic.frequency">
          <strong>Frequency:</strong> {{ kanjidic.frequency }}
        </li>
      </ul>

      <h3>Reading</h3>
      <ul>
        <li v-if="readings.kunyomi">
          <strong>Kunyomi: </strong>
          <div class="japanese">{{ readings.kunyomi }}</div>
        </li>
        <li v-if="readings.onyomi">
          <strong>Onyomi: </strong>
          <div class="japanese">{{ readings.onyomi }}</div>
        </li>
        <li v-if="readings.nanori">
          <strong>Nanori: </strong>
          <div class="japanese">{{ readings.nanori }}</div>
        </li>
      </ul>

      <h3>Meanings</h3>
      <ul>
        <li v-if="meanings.en">
          <strong>English:</strong> {{ meanings.en }}
        </li>
        <li v-if="meanings.es">
          <strong>Spanish:</strong> {{ meanings.es }}
        </li>
        <li v-if="meanings.fr">
          <strong>French:</strong> {{ meanings.fr }}
        </li>
        <li v-if="meanings.pt">
          <strong>Portuguese:</strong> {{ meanings.pt }}
        </li>
        <li v-if="meanings.ko">
          <strong>Korean:</strong> {{ meanings.ko }}
        </li>
        <li v-if="meanings.zh">
          <strong>Chinese (pinyin):</strong> {{ meanings.zh }}
        </li>
        <li v-if="meanings.vi">
          <strong>Vietnamese:</strong> {{ meanings.vi }}
        </li>
      </ul>

      <h3>References</h3>
      <ul>
        <li v-for="key in Object.keys(kanjidic.references)">
          <strong>{{ key }}:</strong> {{ kanjidic.references[key] }}
        </li>
      </ul>

      <h3>Indexing</h3>
      <ul>
        <li v-for="key in Object.keys(kanjidic.query)">
          <strong>{{ key }}:</strong> {{ kanjidic.query[key] }}
        </li>
      </ul>
    </div>

    <div v-else>No data available.</div>
  `,
  data() {
    return {
      loading: true,
      kanjidic: null
    }
  },
  computed: {
    readings() {
      return {
        kunyomi: this.kanjidic.entries
          .flatMap(e => e.reading.ja_kun)
          .join('、'),
        onyomi: this.kanjidic.entries
          .flatMap(e => e.reading.ja_on)
          .join('、'),
        nanori: this.kanjidic.nanori.join('、')
      }
    },
    meanings() {
      return {
        en: this.kanjidic.entries
          .flatMap(e => e.meaning.en)
          .join(', '),
        es: this.kanjidic.entries
          .flatMap(e => e.meaning.es)
          .join(', '),
        fr: this.kanjidic.entries
          .flatMap(e => e.meaning.fr)
          .join(', '),
        pt: this.kanjidic.entries
          .flatMap(e => e.meaning.pt)
          .join(', '),
        ko: this.kanjidic.entries
          .flatMap(e => e.reading.korean_h)
          .join('·'),
        zh: this.kanjidic.entries
          .flatMap(e => e.reading.pinyin)
          .join('、'),
        vi: this.kanjidic.entries
          .flatMap(e => e.reading.vietnam)
          .join(', ')
      }
    }
  },
  created() {
    if (this.kanji.length != 1) {
      this.loading = false
    } else {
      client.kanjidic.getKanji(this.kanji)
        .then((kanjidic) => {
          this.kanjidic = kanjidic
          this.loading = false
        })
    }
  }
}

import KanjiLink from './link.js'

export default {
  name: 'KanjiLinks',
  props: ['kanji'],
  components: {
    KanjiLink
  },
  template: `
    <KanjiLink v-for="character in kanji" :kanji="character" />
  `
}

import client from '../../client.js'
import KanjiLink from './link.js'
import KanjiLinks from './links.js'

const KanjiDependencies = {
  name: 'KanjiDependencies',
  props: ['dependencies'],
  components: {
    KanjiLink
  },
  template: `
    <ul v-if="dependencies">
      <li v-for="dependency in dependencies">
        <KanjiLink :kanji="dependency.kanji" />
        <KanjiDependencies :dependencies="dependency.dependencies" />
      </li>
    </ul>
  `
}

export default {
  name: 'Kanjidep',
  props: ['kanji'],
  components: {
    KanjiLink,
    KanjiLinks,
    KanjiDependencies
  },
  template: `
    <h2>
      <a href="https://gitlab.com/mrpudn/kanjidep.git">Kanjidep</a>
    </h2>

    <div v-if="loading">Loading ...</div>

    <div v-else-if="kanjidep">
      <h3>Dependencies</h3>
      <KanjiDependencies :dependencies="kanjidep.dependencies" />

      <h3>Traversal</h3>
      <KanjiLinks :kanji="kanjidep.traversal" />

      <h3>Dependents</h3>
      <KanjiLinks :kanji="kanjidep.dependents.sort()" />
    </div>

    <div v-else>No data available.</div>
  `,
  data() {
    return {
      loading: true,
      kanjidep: null
    }
  },
  created() {
    if (this.kanji.length != 1) {
      this.loading = false
    }
    client.kanjidep.getKanji(this.kanji)
      .then((kanjidep) => {
        this.kanjidep = kanjidep
        this.loading = false
      })
  }
}

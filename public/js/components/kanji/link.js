
export default {
  name: 'KanjiLink',
  props: ['kanji'],
  template: `
    <a :href="encodeURI('?kanji=' + kanji)" class="japanese">
      {{ kanji }}
    </a>
    &nbsp;
  `
}

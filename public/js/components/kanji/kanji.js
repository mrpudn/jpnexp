import Kanjidic from './kanjidic.js'
import Kanjidep from './kanjidep.js'

export default {
  props: ['params'],
  template: `
    <div v-if="kanji">
      <div>
        <a :href="jishoUri">Jisho.org</a>
        | <a :href="yellowbridgeUri">Yellow Bridge</a>
      </div>

      <div class="japanese" style="font-size: 4em;">
        {{ kanji }}
      </div>

      <div>
        <a :href="kanjiUri">Kanji</a>
        | <a :href="dependenciesUri">Dependencies</a>
      </div>

      <Kanjidic v-if="!view" :kanji="kanji" />

      <Kanjidep v-if="view == 'dependencies'" :kanji="kanji" />
    </div>
    <div v-else>
      No kanji specified.
    </div>
  `,
  components: {
    Kanjidic,
    Kanjidep
  },
  data() {
    return {
      kanji: this.params.get('kanji'),
      view: this.params.get('view')
    }
  },
  computed: {
    kanjiUri() {
      return encodeURI('?kanji=' + this.kanji)
    },
    dependenciesUri() {
      return encodeURI('?kanji=' + this.kanji + '&view=dependencies')
    },
    entriesUri() {
      return encodeURI('?kanji=' + this.kanji + '&view=entries')
    },
    namesUri() {
      return encodeURI('?kanji=' + this.kanji + '&view=names')
    },
    sentencesUri() {
      return encodeURI('?kanji=' + this.kanji + '&view=sentences')
    },
    jishoUri() {
      return 'https://jisho.org/search/' + this.kanji + ' %23kanji'
    },
    yellowbridgeUri() {
      return encodeURI(
        'https://www.yellowbridge.com/chinese/dictionary.php'
          + '?word=' + this.kanji
      )
    }
  }
}

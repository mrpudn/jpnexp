
export default {
  getOrSet(key, expiresIn, fn) {
    return new Promise((resolve, reject) => {
      const item = this.get(key)
      if (item) return resolve(item)
      fn().then(data => {
        this.set(key, data, expiresIn)
        resolve(data)
      })
    })
  },
  get(key) {
    if (this.expire(key)) return null

    const item = JSON.parse(localStorage.getItem(`data.${key}`))
    if (!item) return null

    return item
  },
  set(key, value, expiresIn) {
    localStorage.setItem(`data.${key}`, JSON.stringify(value))

    const expires = new Date()
    expires.setSeconds(expires.getSeconds() + expiresIn)
    localStorage.setItem(`expires.${key}`, JSON.stringify(expires))

    return value
  },
  prune() {
    for (const key in localStorage) {
      if (key.startsWith('expires.')) {
        this.expire(key.slice(8))
      } else if (key.startsWith('data.')) {
        this.expire(key.slice(5))
      }
    }
  },
  expire(key, force = false) {
    if (force || this.expired(key)) {
      localStorage.removeItem(`expires.${key}`)
      localStorage.removeItem(`data.${key}`)
      return true
    }
    return false
  },
  expired(key) {
    const expires = JSON.parse(localStorage.getItem(`expires.${key}`))
    if (!expires) return true

    return new Date() > expires
  }
}

import axios from '..//vendor/axios.min.js'
import cache from './cache.js'

const gitlab = {}
gitlab.version = 'v4'
gitlab.url = `https://gitlab.com/api/${gitlab.version}`

gitlab.fileGetter = (project) => {
  return (filepath) => {
    return new Promise((resolve, reject) => {
      project.instance.get(
        `/repository/files/${encodeURIComponent(filepath)}/raw`,
        { ref: project.ref }
      )
        .then((response) => {
          resolve(response.data)
        })
        .catch((response) => {
          resolve(null)
        })
    })
  }
}

const kanjidic = gitlab.kanjidic = {}
  kanjidic.id = 42180739
  kanjidic.ref = 'main'
  kanjidic.instance = axios.create({
  baseURL: `${gitlab.url}/projects/${kanjidic.id}`
})

kanjidic.getFile = gitlab.fileGetter(kanjidic)

kanjidic.getKanji = (kanji) => {
  return cache.getOrSet(
    `kanjidic.${kanji}`,
    60 * 60,
    () => kanjidic.getFile(`data/kanji/${kanji}.json`)
  )
}

kanjidic.getIndex = (index) => {
  return cache.getOrSet(
    `kanjidic-index.${index}`,
    6 * 60 * 60,
    () => kanjidic.getFile(`data/indexes/${index}.json`)
  )
}

const kanjidep = gitlab.kanjidep = {}
kanjidep.id = 42180441
kanjidep.ref = 'main'
kanjidep.instance = axios.create({
  baseURL: `${gitlab.url}/projects/${kanjidep.id}`
})

kanjidep.getFile = gitlab.fileGetter(kanjidep)

kanjidep.getKanji = (kanji) => {
  return cache.getOrSet(
    `kanjidep.${kanji}`,
    60 * 60,
    () => kanjidep.getFile(`data/kanji/${kanji}.json`)
  )
}

export default gitlab

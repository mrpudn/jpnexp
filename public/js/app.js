import Kanji from './components/kanji/kanji.js'
import Entry from './components/entry/entry.js'
import Name from './components/name/name.js'
import Sentence from './components/sentence/sentence.js'
import Indexes from './components/indexes/indexes.js'
import Home from './components/home/home.js'

export default {
  template: '<component :is="component" :params="params" />',
  data() {
    return {
      params: new URLSearchParams(window.location.search)
    }
  },
  computed: {
    component() {
      if (this.params.has('kanji')) return Kanji
      if (this.params.has('entry')) return Entry
      if (this.params.has('name')) return Name
      if (this.params.has('sentence')) return Sentence
      if (this.params.has('index')) return Indexes
      return Home
    }
  }
}

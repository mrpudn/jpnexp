
export default {
  1: {
    "radical": "一",
    "variant": null,
    "info": "one (いち, ichi, 一)"
  },
  2: {
    "radical": "丨",
    "variant": null,
    "info": "line, stick (ぼう, bō, 棒)"
  },
  3: {
    "radical": "丶",
    "variant": null,
    "info": "dot (てん, ten, 点)"
  },
  4: {
    "radical": "丿",
    "variant": null,
    "info": "bend, possessive particle no (の, no, ノ)"
  },
  5: {
    "radical": "乙",
    "variant": "乚",
    "info": "second, latter (おつ, otsu, 乙)"
  },
  6: {
    "radical": "亅",
    "variant": null,
    "info": "hook, hooked stick (はねぼう, hanebō, 撥棒)"
  },
  7: {
    "radical": "二",
    "variant": null,
    "info": "two (に, ni, 二)"
  },
  8: {
    "radical": "亠",
    "variant": null,
    "info": "pot lid (なべぶた, nabebuta, 鍋蓋)"
  },
  9: {
    "radical": "人",
    "variant": "亻",
    "info": "human (ひと, hito, 人)"
  },
  10: {
    "radical": "儿",
    "variant": null,
    "info": "legs, human underneath (にんにょう, ninnyō, 人繞)"
  },
  11: {
    "radical": "入",
    "variant": "𠆢",
    "info": "enter (いる, iru, 入)"
  },
  12: {
    "radical": "八",
    "variant": "ハ",
    "info": "eight, eight-head (はちがしら, hachigashira, 八頭)"
  },
  13: {
    "radical": "冂",
    "variant": null,
    "info": "inverted box, window frame (まきがまえ, makigamae, 牧構)"
  },
  14: {
    "radical": "冖",
    "variant": null,
    "info": "cover, wa crown (わかんむり, wakanmuri, ワ冠)"
  },
  15: {
    "radical": "冫",
    "variant": null,
    "info": "ice, 2-stroke water (にすい, nisui, 二水)"
  },
  16: {
    "radical": "几",
    "variant": null,
    "info": "desk (つくえ, tsukue, 机)"
  },
  17: {
    "radical": "凵",
    "variant": null,
    "info": "container, inbox (うけばこ, ukebako, 受け箱)"
  },
  18: {
    "radical": "刀",
    "variant": "刂",
    "info": "sword (かたな, katana, 刀)"
  },
  19: {
    "radical": "力",
    "variant": null,
    "info": "power, force (ちから, chikara, 力)"
  },
  20: {
    "radical": "勹",
    "variant": null,
    "info": "embrace, wrap frame (つつみがまえ, tsutsumigamae, 包構)"
  },
  21: {
    "radical": "匕",
    "variant": null,
    "info": "spoon hi (さじのひ, sajinohi, 匕のヒ)"
  },
  22: {
    "radical": "匚",
    "variant": null,
    "info": "box frame (はこがまえ, hakogamae, 匚構)"
  },
  23: {
    "radical": "亡",
    "variant": "匸",
    "info": "dead, hiding frame (かくしがまえ, kakushigamae, 隠構)"
  },
  24: {
    "radical": "十",
    "variant": null,
    "info": "ten, complete (じゅう, jū, 十)"
  },
  25: {
    "radical": "卜",
    "variant": "⼘",
    "info": "divination to (ぼくのと, bokunoto, 卜のト)"
  },
  26: {
    "radical": "卩",
    "variant": "㔾",
    "info": "seal (ふしづくり, fushidzukuri, 節旁)"
  },
  27: {
    "radical": "厂",
    "variant": null,
    "info": "cliff (がんだれ, gandare, 雁垂)"
  },
  28: {
    "radical": "厶",
    "variant": null,
    "info": "private (む, mu)"
  },
  29: {
    "radical": "又",
    "variant": null,
    "info": "again, right hand (また, mata)"
  },
  30: {
    "radical": "口",
    "variant": null,
    "info": "mouth, opening (くち, kuchi)"
  },
  31: {
    "radical": "囗",
    "variant": null,
    "info": "enclosure (くにがまえ, kunigamae, 国構)"
  },
  32: {
    "radical": "土",
    "variant": null,
    "info": "earth (つち, tsuchi)"
  },
  33: {
    "radical": "士",
    "variant": null,
    "info": "scholar, bachelor (さむらい, samurai, 侍)"
  },
  34: {
    "radical": "夂",
    "variant": null,
    "info": "winter (ふゆがしら, fuyugashira, 冬頭)"
  },
  35: {
    "radical": "夊",
    "variant": null,
    "info": "winter variant (すいにょう, suinyou, 夊繞)"
  },
  36: {
    "radical": "夕",
    "variant": null,
    "info": "evening, sunset (ゆうべ, yūbe)"
  },
  37: {
    "radical": "大",
    "variant": null,
    "info": "big, very (だい, dai)"
  },
  38: {
    "radical": "女",
    "variant": null,
    "info": "woman, female (おんな, onna)"
  },
  39: {
    "radical": "子",
    "variant": null,
    "info": "child, seed (こ, ko)"
  },
  40: {
    "radical": "宀",
    "variant": null,
    "info": "roof (うかんむり, ukanmuri, ウ冠)"
  },
  41: {
    "radical": "寸",
    "variant": null,
    "info": "sun (unit of measurement) (すん, sun)"
  },
  42: {
    "radical": "小",
    "variant": "⺌",
    "info": "small, insignificant (ちいさい, chīsai, 小さい)"
  },
  43: {
    "radical": "尢",
    "variant": "尤",
    "info": "lame (まげあし, mageashi)"
  },
  44: {
    "radical": "尸",
    "variant": null,
    "info": "corpse (しかばね, shikabane, 屍)"
  },
  45: {
    "radical": "屮",
    "variant": null,
    "info": "sprout (てつ, tetsu)"
  },
  46: {
    "radical": "山",
    "variant": null,
    "info": "mountain (やま, yama)"
  },
  47: {
    "radical": "巛",
    "variant": "川",
    "info": "river (かわ, kawa)"
  },
  48: {
    "radical": "工",
    "variant": null,
    "info": "work (たくみ, takumi)"
  },
  49: {
    "radical": "已",
    "variant": "己",
    "info": "oneself (おのれ, onore)"
  },
  50: {
    "radical": "巾",
    "variant": null,
    "info": "cloth, turban, scarf (はば, haba)"
  },
  51: {
    "radical": "干",
    "variant": null,
    "info": "dry (ほし, hoshi)"
  },
  52: {
    "radical": "幺",
    "variant": null,
    "info": "short thread (いとがしら, itogashira, 糸頭)"
  },
  53: {
    "radical": "广",
    "variant": null,
    "info": "dotted cliff (まだれ, madare, 麻垂)"
  },
  54: {
    "radical": "廴",
    "variant": null,
    "info": "long stride (いんにょう, innyō, 延繞)"
  },
  55: {
    "radical": "廾",
    "variant": null,
    "info": "two hands, twenty (にじゅうあし, nijūashi, 二十脚)"
  },
  56: {
    "radical": "弋",
    "variant": null,
    "info": "ceremony, shoot, arrow (しきがまえ, shikigamae, 式構)"
  },
  57: {
    "radical": "弓",
    "variant": null,
    "info": "bow (ゆみ, yumi)"
  },
  58: {
    "radical": "彐",
    "variant": "彑",
    "info": "pig's head (けいがしら, keigashira, 彑頭)"
  },
  59: {
    "radical": "彡",
    "variant": null,
    "info": "hair, bristle, stubble, beard (さんづくり, sandzukuri, 三旁)"
  },
  60: {
    "radical": "彳",
    "variant": null,
    "info": "step (ぎょうにんべん, gyouninben, 行人偏)"
  },
  61: {
    "radical": "心",
    "variant": "忄",
    "info": "heart (りっしんべん, risshinben, 立心偏)"
  },
  62: {
    "radical": "戈",
    "variant": null,
    "info": "spear, halberd (かのほこ, kanohoko)"
  },
  63: {
    "radical": "戸",
    "variant": "戶",
    "info": "door, house (とびらのと, tobiranoto)"
  },
  64: {
    "radical": "手",
    "variant": "扌",
    "info": "hand (て, te)"
  },
  65: {
    "radical": "支",
    "variant": null,
    "info": "branch (しにょう, shinyō, 支繞)"
  },
  66: {
    "radical": "攵",
    "variant": "攴",
    "info": "strike, whip (のぶん, nobun, ノ文)"
  },
  67: {
    "radical": "文",
    "variant": null,
    "info": "script, literature (ぶん, bun)"
  },
  68: {
    "radical": "斗",
    "variant": null,
    "info": "dipper, measuring scoop (とます, tomasu)"
  },
  69: {
    "radical": "斤",
    "variant": null,
    "info": "axe (おの, ono, 斧)"
  },
  70: {
    "radical": "方",
    "variant": null,
    "info": "way, square, raft (ほう, hō)"
  },
  71: {
    "radical": "无",
    "variant": "旡",
    "info": "have not (むにょう, munyō)"
  },
  72: {
    "radical": "日",
    "variant": null,
    "info": "sun, day (にち, nichi)"
  },
  73: {
    "radical": "曰",
    "variant": null,
    "info": "say (いわく, iwaku)"
  },
  74: {
    "radical": "月",
    "variant": "⺝",
    "info": "moon, month; body, flesh (つき, tsuki)"
  },
  75: {
    "radical": "木",
    "variant": null,
    "info": "tree (き, ki)"
  },
  76: {
    "radical": "欠",
    "variant": null,
    "info": "yawn, lack (あくび, akubi)"
  },
  77: {
    "radical": "止",
    "variant": null,
    "info": "stop (とめる, tomeru)"
  },
  78: {
    "radical": "歹",
    "variant": "歺",
    "info": "death, decay (がつへん, gatsuhen, 歹偏)"
  },
  79: {
    "radical": "殳",
    "variant": null,
    "info": "weapon, lance (ほこつくり, hokotsukuri)"
  },
  80: {
    "radical": "毋",
    "variant": "母",
    "info": "do not; mother (なかれ-nakare; はは, haha)"
  },
  81: {
    "radical": "比",
    "variant": null,
    "info": "compare, compete (くらべる, kuraberu)"
  },
  82: {
    "radical": "毛",
    "variant": null,
    "info": "fur, hair (け, ke)"
  },
  83: {
    "radical": "氏",
    "variant": null,
    "info": "clan (うじ, uji)"
  },
  84: {
    "radical": "气",
    "variant": null,
    "info": "steam, breath (きがまえ, kigamae, 気構)"
  },
  85: {
    "radical": "水",
    "variant": "氵",
    "info": "water (みず, mizu)"
  },
  86: {
    "radical": "火",
    "variant": "灬",
    "info": "fire (ひ, hi)"
  },
  87: {
    "radical": "爪",
    "variant": "爫",
    "info": "claw, nail, talon (つめ, tsume)"
  },
  88: {
    "radical": "父",
    "variant": null,
    "info": "father (ちち, chichi)"
  },
  89: {
    "radical": "爻",
    "variant": null,
    "info": "mix, twine, cross (こう, kō)"
  },
  90: {
    "radical": "爿",
    "variant": "丬",
    "info": "split wood (しょうへん, shōhen, 爿偏)"
  },
  91: {
    "radical": "片",
    "variant": null,
    "info": "(a) slice (かた, kata)"
  },
  92: {
    "radical": "牙",
    "variant": null,
    "info": "fang (きばへん, kibahen, 牙偏)"
  },
  93: {
    "radical": "牛",
    "variant": "牜",
    "info": "cow (うし, ushi)"
  },
  94: {
    "radical": "犬",
    "variant": "犭",
    "info": "dog (いぬ, inu)"
  },
  95: {
    "radical": "玄",
    "variant": null,
    "info": "dark, profound (げん, gen)"
  },
  96: {
    "radical": "王",
    "variant": "玉",
    "info": "king; ball, jade (おう-ō; たま, tama)"
  },
  97: {
    "radical": "瓜",
    "variant": null,
    "info": "melon (うり, uri)"
  },
  98: {
    "radical": "瓦",
    "variant": null,
    "info": "tile (かわら, kawara)"
  },
  99: {
    "radical": "甘",
    "variant": null,
    "info": "sweet (あまい, amai)"
  },
  100: {
    "radical": "生",
    "variant": null,
    "info": "life (うまれる, umareru)"
  },
  101: {
    "radical": "用",
    "variant": "甩",
    "info": "use; (throw) (もちいる, mochīru)"
  },
  102: {
    "radical": "田",
    "variant": null,
    "info": "field (た, ta)"
  },
  103: {
    "radical": "疋",
    "variant": "⺪",
    "info": "bolt of cloth (ひき, hiki)"
  },
  104: {
    "radical": "疒",
    "variant": null,
    "info": "sickness (やまいだれ, yamaidare, 病垂)"
  },
  105: {
    "radical": "癶",
    "variant": null,
    "info": "footsteps (はつがしら, hatsugashira, 発頭)"
  },
  106: {
    "radical": "白",
    "variant": null,
    "info": "white (しろ, shiro)"
  },
  107: {
    "radical": "皮",
    "variant": null,
    "info": "skin (けがわ, kegawa, 毛皮)"
  },
  108: {
    "radical": "皿",
    "variant": null,
    "info": "dish (さら, sara)"
  },
  109: {
    "radical": "目",
    "variant": null,
    "info": "eye (め, me)"
  },
  110: {
    "radical": "矛",
    "variant": null,
    "info": "spear, pike (むのほこ, munohoko)"
  },
  111: {
    "radical": "矢",
    "variant": null,
    "info": "arrow (や, ya)"
  },
  112: {
    "radical": "石",
    "variant": null,
    "info": "stone (いし, ishi)"
  },
  113: {
    "radical": "示",
    "variant": "礻",
    "info": "altar, display (しめす, shimesu)"
  },
  114: {
    "radical": "禸",
    "variant": null,
    "info": "track (ぐうのあし, gūnoashi)"
  },
  115: {
    "radical": "禾",
    "variant": null,
    "info": "two-branch tree (のぎ, nogi, ノ木)"
  },
  116: {
    "radical": "穴",
    "variant": null,
    "info": "cave (あな, ana)"
  },
  117: {
    "radical": "立",
    "variant": null,
    "info": "stand, erect (たつ, tatsu)"
  },
  118: {
    "radical": "竹",
    "variant": "⺮",
    "info": "bamboo (たけ, take)"
  },
  119: {
    "radical": "米",
    "variant": null,
    "info": "rice (こめ, kome)"
  },
  120: {
    "radical": "糸",
    "variant": "糹",
    "info": "thread, string (いと, ito)"
  },
  121: {
    "radical": "缶",
    "variant": null,
    "info": "can, earthenware jar (かん, kan)"
  },
  122: {
    "radical": "罒",
    "variant": "网",
    "info": "net (あみがしら, amigashira, 網頭)"
  },
  123: {
    "radical": "羊",
    "variant": "⺶",
    "info": "sheep (ひつじ, hitsuji)"
  },
  124: {
    "radical": "羽",
    "variant": null,
    "info": "feather, wing (はね, hane)"
  },
  125: {
    "radical": "耂",
    "variant": "老",
    "info": "old (ろう, rō)"
  },
  126: {
    "radical": "而",
    "variant": null,
    "info": "rake, beard (しかして, shikashite)"
  },
  127: {
    "radical": "耒",
    "variant": null,
    "info": "plow (らいすき, raisuki)"
  },
  128: {
    "radical": "耳",
    "variant": null,
    "info": "ear (みみ, mimi)"
  },
  129: {
    "radical": "聿",
    "variant": "⺻",
    "info": "brush (ふでづくり, fudezukuri, 聿旁)"
  },
  130: {
    "radical": "肉",
    "variant": "⺼",
    "info": "meat (にく, niku)"
  },
  131: {
    "radical": "臣",
    "variant": null,
    "info": "minister, official (しん, shin)"
  },
  132: {
    "radical": "自",
    "variant": null,
    "info": "oneself (みずから, mizukara)"
  },
  133: {
    "radical": "至",
    "variant": null,
    "info": "arrive (いたる, itaru)"
  },
  134: {
    "radical": "臼",
    "variant": null,
    "info": "mortar (うす, usu)"
  },
  135: {
    "radical": "舌",
    "variant": null,
    "info": "tongue (した, shita)"
  },
  136: {
    "radical": "舛",
    "variant": null,
    "info": "opposite (ます, masu)"
  },
  137: {
    "radical": "舟",
    "variant": null,
    "info": "boat (ふね, fune)"
  },
  138: {
    "radical": "艮",
    "variant": null,
    "info": "stopping (うしとら, ushitora, 丑寅)"
  },
  139: {
    "radical": "色",
    "variant": null,
    "info": "colour, prettiness (いろ, iro)"
  },
  140: {
    "radical": "艹",
    "variant": "艸",
    "info": "grass, vegetation (くさ, kusa, 草)"
  },
  141: {
    "radical": "虍",
    "variant": null,
    "info": "tiger stripes (とらかんむり, torakanmuri, 虎冠)"
  },
  142: {
    "radical": "虫",
    "variant": null,
    "info": "insect (むし, mushi)"
  },
  143: {
    "radical": "血",
    "variant": null,
    "info": "blood (ち, chi)"
  },
  144: {
    "radical": "行",
    "variant": null,
    "info": "go, do (ぎょう, gyō)"
  },
  145: {
    "radical": "衣",
    "variant": "衤",
    "info": "clothes (ころも, koromo)"
  },
  146: {
    "radical": "西",
    "variant": "襾",
    "info": "west (にし, nishi)"
  },
  147: {
    "radical": "見",
    "variant": null,
    "info": "see (みる, miru)"
  },
  148: {
    "radical": "角",
    "variant": null,
    "info": "horn (つの, tsuno)"
  },
  149: {
    "radical": "言",
    "variant": "訁",
    "info": "speech (こと, koto)"
  },
  150: {
    "radical": "谷",
    "variant": null,
    "info": "valley (たに, tani)"
  },
  151: {
    "radical": "豆",
    "variant": null,
    "info": "bean (まめ, mame)"
  },
  152: {
    "radical": "豕",
    "variant": null,
    "info": "pig (いのこ, inoko, 猪子)"
  },
  153: {
    "radical": "豸",
    "variant": null,
    "info": "cat, badger (むじな, mujina, 狢)"
  },
  154: {
    "radical": "貝",
    "variant": null,
    "info": "shell (かい, kai)"
  },
  155: {
    "radical": "赤",
    "variant": null,
    "info": "red, bare (あか, aka)"
  },
  156: {
    "radical": "走",
    "variant": "赱",
    "info": "run (はしる, hashiru)"
  },
  157: {
    "radical": "足",
    "variant": "⻊",
    "info": "foot (あし, ashi)"
  },
  158: {
    "radical": "身",
    "variant": null,
    "info": "body (み, mi)"
  },
  159: {
    "radical": "車",
    "variant": null,
    "info": "cart, car (くるま, kuruma)"
  },
  160: {
    "radical": "辛",
    "variant": null,
    "info": "spicy, bitter (からい, karai)"
  },
  161: {
    "radical": "辰",
    "variant": null,
    "info": "morning (しんのたつ, shinnotatsu, 辰のたつ)"
  },
  162: {
    "radical": "⻌",
    "variant": "辵",
    "info": "walk (しんにゅう, shinnyū, 之繞)"
  },
  163: {
    "radical": "邑",
    "variant": "⻏",
    "info": "town (阝 right) (むら, mura)"
  },
  164: {
    "radical": "酉",
    "variant": null,
    "info": "sake (rice-based alcoholic beverage) (とり, tori)"
  },
  165: {
    "radical": "釆",
    "variant": null,
    "info": "divide, distinguish, choose (のごめ, nogome, ノ米)"
  },
  166: {
    "radical": "里",
    "variant": null,
    "info": "village, mile (さと, sato)"
  },
  167: {
    "radical": "金",
    "variant": "釒",
    "info": "metal, gold (かね, kane)"
  },
  168: {
    "radical": "長",
    "variant": "镸",
    "info": "long, grow; leader (ながい-nagai; ちょう, chō)"
  },
  169: {
    "radical": "門",
    "variant": null,
    "info": "gate (もん, mon)"
  },
  170: {
    "radical": "阜",
    "variant": "⻖",
    "info": "mound, dam (阝 left) (ぎふのふ, gifunofu, 岐阜の阜)"
  },
  171: {
    "radical": "隶",
    "variant": null,
    "info": "slave, capture (れいづくり, reidzukuri, 隷旁)"
  },
  172: {
    "radical": "隹",
    "variant": null,
    "info": "old bird (ふるとり, furutori, 古鳥)"
  },
  173: {
    "radical": "雨",
    "variant": "⻗",
    "info": "rain (あめ, ame)"
  },
  174: {
    "radical": "青",
    "variant": "靑",
    "info": "green, blue (あお, ao)"
  },
  175: {
    "radical": "非",
    "variant": null,
    "info": "wrong (あらず, arazu)"
  },
  176: {
    "radical": "面",
    "variant": "靣",
    "info": "face (めん, men)"
  },
  177: {
    "radical": "革",
    "variant": null,
    "info": "leather, rawhide (かくのかわ, kakunokawa)"
  },
  178: {
    "radical": "韋",
    "variant": null,
    "info": "tanned leather (なめしがわ, nameshigawa)"
  },
  179: {
    "radical": "韭",
    "variant": null,
    "info": "leek (にら, nira)"
  },
  180: {
    "radical": "音",
    "variant": null,
    "info": "sound (おと, oto)"
  },
  181: {
    "radical": "頁",
    "variant": null,
    "info": "big shell (おおがい, ōgai, 大貝)"
  },
  182: {
    "radical": "風",
    "variant": "𠘨",
    "info": "wind (かぜ, kaze)"
  },
  183: {
    "radical": "飛",
    "variant": null,
    "info": "fly (とぶ, tobu)"
  },
  184: {
    "radical": "食",
    "variant": "飠",
    "info": "eat, food (しょく, shoku)"
  },
  185: {
    "radical": "首",
    "variant": null,
    "info": "neck, head (くび, kubi)"
  },
  186: {
    "radical": "香",
    "variant": null,
    "info": "fragrant (においこう, nioikō)"
  },
  187: {
    "radical": "馬",
    "variant": null,
    "info": "horse (うま, uma)"
  },
  188: {
    "radical": "骨",
    "variant": null,
    "info": "bone (ほね, hone)"
  },
  189: {
    "radical": "高",
    "variant": "髙",
    "info": "tall, high (たかい, takai)"
  },
  190: {
    "radical": "髟",
    "variant": null,
    "info": "hair (かみがしら, kamigashira, 髪頭)"
  },
  191: {
    "radical": "鬥",
    "variant": null,
    "info": "fight (とうがまえ, tōgamae, 闘構)"
  },
  192: {
    "radical": "鬯",
    "variant": null,
    "info": "herbs, sacrificial wine (ちょう, chō)"
  },
  193: {
    "radical": "鬲",
    "variant": null,
    "info": "tripod, cauldron (かなえ, kanae)"
  },
  194: {
    "radical": "鬼",
    "variant": null,
    "info": "ghost, demon (おに, oni)"
  },
  195: {
    "radical": "魚",
    "variant": null,
    "info": "fish (うお, uo)"
  },
  196: {
    "radical": "鳥",
    "variant": null,
    "info": "bird (とり, tori)"
  },
  197: {
    "radical": "鹵",
    "variant": null,
    "info": "salt (ろ, ro)"
  },
  198: {
    "radical": "鹿",
    "variant": null,
    "info": "deer (しか, shika)"
  },
  199: {
    "radical": "麦",
    "variant": "麥",
    "info": "wheat (むぎ, mugi, 麦)"
  },
  200: {
    "radical": "麻",
    "variant": null,
    "info": "hemp, flax (あさ, asa)"
  },
  201: {
    "radical": "黄",
    "variant": "黃",
    "info": "yellow (きいろ, kīro, 黄色)"
  },
  202: {
    "radical": "黍",
    "variant": null,
    "info": "millet (きび, kibi)"
  },
  203: {
    "radical": "黒",
    "variant": "黑",
    "info": "black (くろ, kuro)"
  },
  204: {
    "radical": "黹",
    "variant": null,
    "info": "embroidery, needlework (ふつ, futsu, 黻)"
  },
  205: {
    "radical": "黽",
    "variant": null,
    "info": "frog, amphibian (べん, ben)"
  },
  206: {
    "radical": "鼎",
    "variant": null,
    "info": "sacrificial tripod (かなえ, kanae)"
  },
  207: {
    "radical": "鼓",
    "variant": null,
    "info": "drum (つづみ, tsudzumi)"
  },
  208: {
    "radical": "鼠",
    "variant": null,
    "info": "rat, mouse (ねずみ, nezumi)"
  },
  209: {
    "radical": "鼻",
    "variant": null,
    "info": "nose (はな, hana)"
  },
  210: {
    "radical": "齊",
    "variant": null,
    "info": "even, uniformly (せい, sei, 斉)"
  },
  211: {
    "radical": "歯",
    "variant": "齒",
    "info": "tooth, molar (は, ha)"
  },
  212: {
    "radical": "竜",
    "variant": "龍",
    "info": "dragon (りゅう, ryū)"
  },
  213: {
    "radical": "亀",
    "variant": "龜",
    "info": "turtle, tortoise (かめ, kame)"
  },
  214: {
    "radical": "龠",
    "variant": null,
    "info": "flute (やく, yaku)"
  }
}

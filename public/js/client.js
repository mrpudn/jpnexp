import gitlab from './client/gitlab.js'

export default {
  kanjidic: gitlab.kanjidic,
  kanjidep: gitlab.kanjidep
}
